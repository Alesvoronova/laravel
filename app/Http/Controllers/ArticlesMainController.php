<?php

namespace App\Http\Controllers;

use App\Model\Article;
use App\Model\Category;

class ArticlesMainController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('id','asc')->paginate(3);
        $categories = Category::all();
        return view('index', ['articles' => $articles,'categories' => $categories]);
    }

    public function sort($id)
    {
        $category = Category::find($id);
        $articles = $category->articles()->paginate(2);
        $categories = Category::all();
        return view('index', ['articles' => $articles, 'categories' => $categories]);
    }
}
