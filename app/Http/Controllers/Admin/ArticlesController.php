<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Model\Article;
use App\Model\Category;
use App\Model\CategoryArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ArticlesController extends Controller
{

    public function index()
    {
        $objArticle = new Article();
        $articles = $objArticle->get();
        return view('admin.articles.index', ['articles' => $articles]);
    }

    public function add()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        return view('admin.articles.add', ['categories' => $categories]);
    }

    public function addPost(ArticleRequest $request)
    {
        $obgArticle = new Article();
        if(Gate::denies('add',$obgArticle)){
            return back()->with('message', 'You do not have rights');
        }
        $article = $obgArticle->create([
            'title' => $request->title,
            'description' => $request->description
        ]);
        if ($article) {
            foreach ($request->categories as $category_id) {
                $categoryArticle = new CategoryArticle();
                $categoryArticle->category_id = $category_id;
                $categoryArticle->article_id = $article->id;
                $categoryArticle->save();
            }

            return redirect()->route('articles');
        }
        return back()->with('message', 'Article not added');
    }

    public function edit($id)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        if (!$article = Article::find($id)) {
            return abort(404);
        }
        $arrayCategories = [];
        $mainCategories = $article->categories;
        foreach ($mainCategories as $category) {
            $arrayCategories[] = $category->id;
        }
        return view('admin.articles.edit', [
            'categories' => $categories,
            'article' => $article,
            'arrayCategories' => $arrayCategories
            ]);
    }

    public function editPost(ArticleRequest $request, $id)
    {
        if (!$article = Article::find($id)) {
            return abort(404);
        }
        if(Gate::denies('add',$article)){
            return back()->with('message', 'You do not have rights');
        }
        $article->title = $request->title;
        $article->description = $request->description;
        if ($article->save()) {
            $categoryArticle = new CategoryArticle();
            $categoryArticle->where('article_id',$article->id)->delete();
            $arrayCategories = $request->categories;
            foreach ($arrayCategories as $category) {
                $categoryArticle = new CategoryArticle();
                $categoryArticle->category_id = $category;
                $categoryArticle->article_id = $article->id;
                $categoryArticle->save();
            }
            return redirect()->route('articles');
        }

        return back()->with('message', 'Article not added');

    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $id = (int)$request->id;
            $article = Article::findOrFail($id);

            $article->delete();
            return "Article deleted";
        }

    }
}
