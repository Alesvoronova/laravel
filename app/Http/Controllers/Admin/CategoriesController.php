<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{

    public function index()
    {
        $objCategory = new Category();
        $categories = $objCategory->get();
        return view('admin.categories.index', ['categories' => $categories]);
    }

    public function add()
    {
        return view('admin.categories.add');
    }

    public function addPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string','min:3', 'max:1000'],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $category = new Category();
        $category->title = $request->title;
        $category->description = $request->description;
        if ($category->save()) {

            return redirect()->route('categories');
        }

        return back()->with('message', 'Category not added');
    }

    public function edit($id)
    {
        if (!$category = Category::find($id)) {
            return abort(404);
        }

        return view('admin.categories.edit', ['category' => $category]);
    }

    public function editPost(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'min:3', 'max:1000'],
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        if (!$category = Category::find($id)) {
            return abort(404);
        }
        $category->title = $request->title;
        $category->description = $request->description;
        if ($category->save()) {

            return redirect()->route('categories');
        }

        return back()->with('message', 'Category not edited');

    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            $id = (int)$request->id;
            $category = Category::findOrFail($id);

            $category->delete();
            return "Category deleted";
        }

    }
}
