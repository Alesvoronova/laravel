<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Model\Comment;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{

    public function addPost(CommentRequest $request)
    {
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->article_id = $request->article_id;
        $comment->user_id = Auth::user()->id;
        if($comment->save()){
            return redirect()->route('main');
        }
        return back()->with('message', 'Comment not added');
    }
}
