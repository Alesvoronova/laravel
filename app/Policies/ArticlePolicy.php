<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function add(User $user){
        if($user->name == 'Super Admin'){
            return true;
        }
        return false;
    }

    public function update(User $user){

        if($user->name == 'Super Admin'){
            return true;
        }
        return false;
    }
}
