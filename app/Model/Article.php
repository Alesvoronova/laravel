<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primaryKey = "id";

    protected $fillable = [
        'title', 'description', 'image'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_articles', 'article_id', 'category_id');
    }

    public function comments(){

        return $this->hasMany(Comment::class,'article_id','id');
    }
}
