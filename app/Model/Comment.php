<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $primaryKey = "id";

    protected $fillable = [
        'article_id', 'user_id', 'comment','status'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function user(){

        return $this->belongsTo(User::class,'user_id','id');
    }
}
