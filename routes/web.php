<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses' =>'ArticlesMainController@index'])->name('main');

Route::get('/{id}',['uses' =>'ArticlesMainController@sort'])->where('id','\d+')->name('sort');


Route::group(['middleware'=> 'guest'], function () {
    Route::get('/register',['uses' =>'Auth\RegisterController@showRegistrationForm'])->name('register');
    Route::post('/register',['uses' =>'Auth\RegisterController@register']);
    Route::get('/login',['uses' =>'Auth\LoginController@showLoginForm'])->name('login');
    Route::post('/login',['uses' =>'Auth\LoginController@login']);
});

Route::group(['middleware'=>'auth'], function () {
    Route::post('/logout',['uses' =>'Auth\LoginController@logout'])->name('logout');
    Route::post('/comments/add',['uses' =>'CommentsController@addPost'])->name('comments.add');
});

Route::group(['middleware'=> ['admin', 'auth'],'prefix'=>'admin'], function () {
    Route::get('/',['uses' =>'Admin\AdminDashboardController@index'])->name('admin');
    /** Categories */
    Route::get('/categories',['uses' =>'Admin\CategoriesController@index'])->name('categories');
    Route::get('/categories/add',['uses' =>'Admin\CategoriesController@add'])->name('categories.add');
    Route::post('/categories/add',['uses' =>'Admin\CategoriesController@addPost']);
    Route::get('/categories/edit/{id}',['uses' =>'Admin\CategoriesController@edit'])
        ->where('id','\d+')
        ->name('categories.edit');
    Route::post('/categories/edit/{id}',['uses' =>'Admin\CategoriesController@editPost'])
        ->where('id','\d+');
    Route::post('/categories/delete',['uses' =>'Admin\CategoriesController@delete'])->name('categories.delete');
    /** Articles */
    Route::get('/articles',['uses' =>'Admin\ArticlesController@index'])->name('articles');
    Route::get('/articles/add',['uses' =>'Admin\ArticlesController@add'])->name('articles.add');
    Route::post('/articles/add',['uses' =>'Admin\ArticlesController@addPost']);
    Route::get('/articles/edit/{id}',['uses' =>'Admin\ArticlesController@edit'])
        ->where('id','\d+')
        ->name('articles.edit');
    Route::post('/articles/edit/{id}',['uses' =>'Admin\ArticlesController@editPost'])
        ->where('id','\d+');
    Route::post('/articles/delete',['uses' =>'Admin\ArticlesController@delete'])->name('articles.delete');

    /** Users */
    Route::get('/users',['uses' =>'Admin\UsersController@index'])->name('users');
});


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

