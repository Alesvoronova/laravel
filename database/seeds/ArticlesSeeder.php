<?php

use App\Model\Article;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::create([
              'title' => 'Man must explore, and this is exploration at its greatest',
                'description' => 'Problems look mighty small from 150 miles up',
        ]);

        Article::create([
            'title' => 'Science has not yet mastered prophecy',
            'description' => 'We predict too much for the next year and yet far too little for the next ten.',
        ]);

        Article::create([
            'title' => 'Failure is not an option',
            'description' => 'Many say exploration is part of our destiny, but it’s actually our duty to future generations.',
        ]);

        Article::create([
            'title' => 'I believe every human has a finite number of heartbeats. I don\'t intend to waste any of mine.',
            'description' => 'Problems look mighty small from 150 miles up',
        ]);
    }
}
