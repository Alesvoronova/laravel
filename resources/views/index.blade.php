@extends('layouts.main')

@section('content')
    <!-- Page Header -->
    <header class="masthead" style="background-image: url('blog/img/home-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Blog</h1>
                        {{--<span class="subheading">Blog</span>--}}
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                @foreach($articles as $article)
                    <div class="post-preview">
                        <a href="blog/post.html">
                            <h2 class="post-title">
                                {!! $article->title !!}
                            </h2>
                            <h3 class="post-subtitle">
                                {!! $article->description !!}
                            </h3>
                        </a>
                        <p class="post-meta">Posted on {!! $article->created_at->format('d/m/Y') !!}</p>
                        @foreach($article->comments as $comment)
                            <h3 class="post-meta">Comment: {!!$comment->user->name!!}
                            </h3>
                            <div class="col-lg-8">
                                <h4 class="post-meta">{!! $comment->comment !!}
                                </h4>
                            </div>
                        @endforeach
                        @if(Session::has('message'))
                            <div class="alert alert-danger">
                                {{session('message')}}
                            </div>
                        @endif
                        @if(Auth::check())
                            <br>
                            <form method="post" action="{{ route('comments.add') }}">
                                @csrf
                                <input type="hidden" value="{{$article->id}}" name="article_id">
                                <div class="col-lg-8">

                                    <label for="comment" class="col-form-label">{{ __('Comment') }}</label>
                                </div>
                                <div class="col-lg-8">
                                    <textarea id="comment" type="text" name="comment"
                                              class="form-control @error('comment') is-invalid @enderror"></textarea>
                                    @error('comment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                     </span>
                                    @enderror
                                </div>
                                <br>
                                <div class="col-lg-8">
                                    <button type="submit" class="btn btn-dark">
                                        {{ __('Add') }}
                                    </button>
                                </div>
                            </form>
                        @endif
                    </div>
            @endforeach
            <!-- Pager -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-end">
                            <li class="page-item">
                                {{ $articles->appends(['id' => 'ASC/DESC'])->links() }}
                            </li>
                        </ul>
                    </nav>
            </div>
        </div>
    </div>

    <hr>
    <br>
    <br>
    <div class="col-lg-6 col-md-10 mx-auto">
        <div class="btn-group mr-2">
            <a href="{{ route('main') }}">
                <button class="btn btn-sm btn-outline-secondary"> {{ __('All category') }}</button>
            </a>
        </div>
    </div>
    <br>
    <br>
    @foreach($categories as $category)
        <div class="col-lg-6 col-md-10 mx-auto">
            <div class="btn-group mr-2">
                <a href="{{ route('sort',['id'=>$category->id]) }}">
                    <button class="btn btn-sm btn-outline-secondary"> {{ __($category->title) }}</button>
                </a>
            </div>
        </div>
        <br>
        <br>
    @endforeach
@endsection