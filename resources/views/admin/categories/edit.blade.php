@extends('layouts.admin')

@section('content')

    <main class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

        <div class="card-body">
            <h1 class="h2">Edit categories</h1>
            @if(Session::has('message'))
                <div class="alert alert-danger">
                    {{session('message')}}
                </div>
            @endif
            <form method="post">
                @csrf

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="title" class="col-form-label">{{ __('Title category') }}</label>

                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                               name="title" value="{{ $category->title }}" required autocomplete="title" autofocus>

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="description" class="col-form-label ">{{ __('Description category') }}</label>


                        <textarea id="description" class="form-control @error('description') is-invalid @enderror"
                                  name="description" type="text" required
                                  autocomplete="description" autofocus>{!! $category->description !!}
                        </textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark">
                    {{ __('Edit') }}
                </button>
            </form>
        </div>
    </main>
@endsection
