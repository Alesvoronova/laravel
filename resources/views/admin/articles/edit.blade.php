@extends('layouts.admin')

@section('content')

    <main class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">

        <div class="card-body">
            <h1 class="h2">Edit article</h1>
            @if(Session::has('message'))
                <div class="alert alert-danger">
                    {{session('message')}}
                </div>
            @endif
            <form method="post">
                @csrf
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="title" class="col-form-label">{{ __('Select several categories') }}</label>

                        <select name="categories[]" id="categories" class="form-control" multiple>
                            @foreach($categories as $key => $category)
                                <option value="{{$key}}"@if(in_array( $key, $arrayCategories)) selected @endif>{{$category}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="title" class="col-form-label">{{ __('Title article') }}</label>

                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                               name="title" value="{{ $article->title  }}" required autocomplete="title" autofocus>

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="description" class="col-form-label ">{{ __('Description article') }}</label>


                        <textarea id="description" class="form-control @error('description') is-invalid @enderror"
                                  name="description" type="text" required
                                  autocomplete="description" autofocus>{!! $article->description !!}
                        </textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-dark">
                    {{ __('Edit') }}
                </button>
            </form>
        </div>
    </main>
@endsection
@section('select2')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#categories').select2();
        });
    </script>
@endsection