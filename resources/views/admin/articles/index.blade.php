@extends('layouts.admin')
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <div class="btn-group mr-2">
                <a href="{{ route('articles.add') }}">
                    <button class="btn btn-sm btn-outline-secondary"> {{ __('Add article') }}</button>
                </a>
            </div>
        </div>

        <h2>Articles</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td>{{$article->id}}</td>
                        <td>{{$article->title}}</td>
                        <td>{!! $article->description !!}</td>
                        <td>{{$article->created_at->format('d-m-Y H:i')}}</td>
                        <td><a href="{{ route('articles.edit', ['id'=>$article->id]) }}" class="btn btn-dark">Edit</a>
                            <a href="javascript:void(0)" id = "delete-article" data-id = "{{ $article->id }}" class="btn btn-danger delete-article">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </main>
@stop
@section('js')
    <script>
        $('body').on('click', '.delete-article', function() {
            if(confirm("Are You sure want to delete ?")){
                let id = $(this).data("id");
                $.ajax({
                    type: 'POST',
                    url: "{{ route('articles.delete') }}",
                    data: {id:id},
                    success: function(data) {
                        alert(data);
                        location.reload();
                    }
                })
            }else{
                alertify.errors('Action canceled user');
            }
        });
    </script>
@endsection